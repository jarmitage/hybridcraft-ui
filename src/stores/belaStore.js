import { writable } from "svelte/store"
import Bela from '../lib/Bela/Bela.js'

const createBela = () => {

    let bela = {}

    let state = {
        connected: false,
        ip: '192.168.7.2',
        interval: 500
    }

    const { subscribe, set, update } = writable(state)

    const methods = {
        init: (_ip=null, _onControl=null, _onData=null, _onConnect=null, _onDisconnect=null) => {
            if (_ip        != null) methods.updateIp(_ip)
            if (_onControl != null) methods.onControl = _onControl
            if (_onData    != null) methods.onData    = _onData
            if (_onConnect    != null) methods.onConnect    = _onConnect
            if (_onDisconnect != null) methods.onDisconnect = _onDisconnect
            bela = new Bela(methods.getIp())
            bela.control.target.addEventListener('custom', function(event){
                methods.onControl(event)
            })
            // TODO:
            // bela.data.target.addEventListener('custom', function(data){
            //     methods.onData(data)
            // })
            methods.monitor(bela)
        },
        monitor: (b) => {
            let interval
            update (s => { interval = s.interval; return s })
            setInterval(function() {
                if (b.isConnected() && !methods.connected()) {
                    update(s => { s.connected = true;  return s} )
                    methods.onConnect()
                }
                if (!b.isConnected() && methods.connected()){
                    update(s => { s.connected = false; return s} )
                    methods.onDisconnect()
                }
            }, interval)
        },
        updateIp: (_ip) => {
            if (!methods.connected())
                update (s => { s.ip = _ip; return s })
        },
        getIp : () => {
            let ip
            update (s => { ip = s.ip; return s })
            return ip
        },
        connected: () => {
            let connected
            update (s=> { connected = s.connected; return s })
            return connected
        },
        onConnect: () => {
            console.log('[BelaStore] Connected.')
        },
        onDisconnect: () => {
            console.log('[BelaStore] Disconnected.')
        },
        sendControl: (msg) => {
            if (bela.isConnected()) bela.control.send(msg)
        },
        sendData: (msg) => {
            if (bela.isConnected()) bela.data.send(msg)
        },
        onControl: (event) => {
            console.log('[BelaStore] received event:', event.detail)
        },
        onData: (data) => {
            console.log('[BelaStore] received data:', data.detail)
        },
        reset: () => {
            bela = {}
            return set(state)
        }
    }

    return {
        subscribe,
        set,
        update,
        ...methods
    }
}

export const BelaStore = createBela()
