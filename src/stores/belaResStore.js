import { writable } from "svelte/store"
import { BelaStore as Bela } from './belaStore.js'

const createBelaRes = () => {

	let debug = false

	let state = {
		blocks:         4,
		defaultModels:  [],
		defaultPitches: [],
	}

	const { subscribe, set, update } = writable(state)
	
	const methods = {
		init:     () => { /*  */ },
		setModels: (model) => {
			let b
			update(s => { b = s.blocks; return s })
			for (var i = 0; i < b; i++)
				methods.setModel(i, model)
		},
		setModel: (index, model) => {
			let msg = {
				command: 'updateModel',
				args: {
					index: methods.checkIndex(index),
					model: model
				}
			}
			if (Bela.connected()) {
				if (debug) console.log('[BelaResStore] setModel(): msg', msg)
				Bela.sendControl(msg)
			}
		},
		setPitches: (pitch) => {
			let b
			update(s => { b = s.blocks; return s })
			for (var i = 0; i < b; i++)
				methods.setPitch(0, pitch)
		},
		setPitch: (index, pitch) => {
			let msg = {
				command: 'updatePitch', 
				args: {
					index: methods.checkIndex(index),
					pitch: pitch
				}
			}
			if (Bela.connected()) {
				if (debug) console.log('[BelaResStore] setPitch(): msg', msg)
				Bela.sendControl(msg)
			}
		},
		setResonator: (bankIndex, resIndex, params) => {
			let msg = {
				command: 'updateResResonator',
				args: {
					bankIndex: bankIndex, 
					resIndex:  resIndex, 
					params:    params
				}
			}
			if (Bela.connected()) Bela.sendControl(msg)
		},
		setResonatorParam: (bankIndex, resIndex, paramIndex, value) => {
			let msg = {
				command: 'updateResResonatorParam', 
				args: {
					bankIndex:  bankIndex, 
					resIndex:   resIndex, 
					paramIndex: paramIndex, 
					value:      value
				}
			}
			if (Bela.connected()) Bela.sendControl(msg)
		},
		// setResonators: () => {},
		checkIndex: (index) => {
			update(s => { 
				if (index > s.blocks-1) {
					console.log('[BelaResonatorsStore] Warning! Block index out of bounds:', index)
					index = s.blocks
				}
				return s
			})
			return index
		},
		reset: () => { return set(state) }
	}

	return { subscribe, set, update, ...methods }

}

export const BelaResStore = createBelaRes()
