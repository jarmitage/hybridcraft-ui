import { writable } from 'svelte/store'
import { APIStore as API } from './apiStore.js'
import { BelaStore as Bela } from './belaStore.js'
import { BelaResStore as BelaRes } from './belaResStore.js'
import * as utils from '../lib/Utils/utils.js'

const createSession = () => {

	let debug = true

	let sculpture = {
		setup: {
			baseModel: {
				metadata: {
					name: '', 
					fundamental: 0, 
					resonators: 0
				},
				resonators: []
			},
			timestamp: '',
			freqResponse: {}
		},
		sculpts: [/*{
			id: '',
			timestamp: '',
			freqResponse: {},
			diff: {},
			model: {
				metadata: {
					name: '', 
					fundamental: 0, 
					resonators: 0 },
				resonators: []
			}
		}*/],
		hide: false
	}

	let blocks = [
		{ locked: false, sculptId: "", pitch: "c3", fx: {} },
		{ locked: false, sculptId: "", pitch: "a3", fx: {} },
		{ locked: false, sculptId: "", pitch: "g4", fx: {} },
		{ locked: false, sculptId: "", pitch: "d5", fx: {} }
	]

	let instrument = {
		mode:   {
			play:    true,
			compare: false,
			preview: false
		},
		blocks: JSON.parse(JSON.stringify(blocks))
	}

	let session = {
		timestamp:   '',
		path:        '',
		sculptures:  [JSON.parse(JSON.stringify(sculpture))],
		survey:      {},
		transcript:  {},
		annotations: {},
		instrument:  JSON.parse(JSON.stringify(instrument)),
		instrumentLog: []
	}

	let state = {
		db:         [],
		baseModels: [],
		selectedDbEntry:   0,
		selectedBaseModel: 0,
		selectedSculpture: 0,
		selectedSculpts:   [[[]]],
		sculptInProgress:  false,
		sessionDidLoad:    false,
		session: JSON.parse(JSON.stringify(session)),
	}

	const { subscribe, set, update } = writable(state)

	const methods = {
		// Session management
		init: async (belaOnControl, belaOnData, belaOnConnect, belaOnDisconnect) => {
			API.init()
			Bela.init(
				'192.168.8.2',
				belaOnControl, 
				belaOnData,
				belaOnConnect,
				belaOnDisconnect
			)
			let restoredSession = await methods.restoreSession()
			if (debug) console.log('[SessionStore] init(): Finished.')
			return restoredSession
		},
		restoreSession: async () => {
			let res = await API.req('restore')
			if (debug) console.log('[SessionStore] restoreSession(): res', res)
			update(s => {
				s.db         = res.db
				s.baseModels = res.baseModels
				s.session    = res.session
				s.sessionDidLoad = true
				return s
			})
			methods.syncBlocksToBela()
			return res
		},
		syncBlocksToBela: async () => {
			// get blocks and send to bela
			let _blocks
			update(s => { _blocks = s.session.instrument.blocks; return s })
			if (debug) console.log('[SessionStore] syncBlocksToBela(): blocks:', _blocks)
			for (var i = 0; i < _blocks.length; i++) {
				let _model = methods.getModelBySculptId(_blocks[i].sculptId)
				BelaRes.setPitch(i, _blocks[i].pitch)
				if (_model) BelaRes.setModel(i, _model)
			}
			return _blocks
		},
		saveInstrument: async () => {
			let _instrument, _blocks
			update(s => {
				_instrument = s.session.instrument
				return s 
			})
			let res = await API.req('saveInstrument', { instrument: JSON.stringify(_instrument) })
			if (debug) console.log('[SessionStore] saveInstrument(): _instrument, res', _instrument, res.instrument)
			return res
		},
		// TODO: resetInstrument that includes FX
		new: async () => {
			let res = await API.req('new')
			if (debug) console.log('[SessionStore] new(): res', res)
			update(s => {
				s.db         = res.db
				s.baseModels = res.baseModels
				s.session    = res.session
				s.selectedDbEntry = s.db[s.db.length-1]
				s.sessionDidLoad = true
				return s
			})
			methods.newBlocks()
			return res
		},
		newBlocks: () => {
			for (var i = 0; i < blocks.length; i++) {
				methods.setBlockToBaseModel(i, 0)
				BelaRes.setPitch(i, blocks[i].pitch)
			}
		},
		load: async (dbIndex) => {
			let timestamp
			update(s => { timestamp = s.db[dbIndex]; return s })
			let res = await API.req('load', {timestamp: timestamp})
			if (debug) console.log('[SessionStore] load(): res', res)
			update(s => {
				s.session    = res
				s.selectedDbEntry = s.db[s.db.length-1]
				s.sessionDidLoad = true
				return s
			})
			return res
		},
		// Sculpting
		sculptureSetup: async (baseModel) => {
			if (!methods.sculptInProgress()) {
				methods.sculptStarted()
				let res = await API.req('setup', {baseModel: baseModel})
				if (debug) console.log('[SessionStore] sculptureSetup(): res', res)
				update(s => { s.session = res; return s })
				methods.sculptEnded()
				return res
			}
		},
		sculptureSculpt: async () => {
			if (!methods.sculptInProgress()) {
				methods.sculptStarted()
				let res = await API.req('sculpt')
				if (debug) console.log('[SessionStore] sculptureSculpt(): res', res)
				update(s => { s.session = res; return s }) // update session
				methods.sculptEnded()
				return res
			}
		},
		sculptureSelect: async (index) => {
			let res = await API.req('select', {index: index})
			if (debug) console.log('[SessionStore] sculptureSelect(): res', res)
			update(s => { s.session = res; return s })
			return res
		},
		sculptureHide: async (index) => {
			let res = await API.req('hide', {index: index})
			if (debug) console.log('[SessionStore] sculptureHide(): res', res)
			update(s => { s.session = res; return s })
			return res	
		},
		sculptureUnhide: async (index) => {
			let res = await API.req('unhide', {index: index})
			if (debug) console.log('[SessionStore] sculptureUnhide(): res', res)
			update(s => { s.session = res; return s })
			return res	
		},
		sculptureAddTmp: () => {
			let setup_state = methods.sculptureSetupState()
			if (!setup_state) {
				let tmp = sculpture
				update(s => {
					s.selectedBaseModel = 0
					s.session.sculptures.push(tmp)
					s.selectedSculpture = s.session.sculptures.length-1
					return s
				})
				methods.setInstrumentMode('preview')
				return tmp
			} else {
				if (debug) console.log('[SessionStore] sculptureAddTmp(): Sculpture already added!')
			}
		},
		// Sculpt progress
		sculptStarted: () => {
			update (s => { s.sculptInProgress = true; return s })
			console.log('[SessionStore] Sculpt started.')
		},
		sculptEnded: () => {
			update (s => { s.sculptInProgress = false; return s })
			console.log('[SessionStore] Sculpt ended.')
		},
		sculptInProgress: () => {
			let status
			update (s => { status = s.sculptInProgress; return s })
			if (status) console.log("[SessionStore] Sculpt already in progress.")
			return status
		},
		sculptureIndexFromId: (id) => {
			if (utils.isNonEmptyStr(id)) return parseInt(id.substring(0,2))
			else return undefined
		},
		sculptIndexFromId: (id) => {
			if (utils.isNonEmptyStr(id)) return parseInt(id.substring(2,4))
			else return undefined
		},
		setSelectedBaseModel: (index) => {
			update(s => { s.selectedBaseModel = index; return s})
		},
		getSelectedBaseModel: () => {
			let _selectedBaseModel
			update(s => { _selectedBaseModel = s.selectedBaseModel; return s })
			let _baseModel
			if (_selectedBaseModel < 4)
				update(s => { _baseModel = s.baseModels[s.selectedBaseModel]; return s })
			else
				_baseModel = methods.getSculptModelByBlockIndex(_selectedBaseModel-4)
			return _baseModel
		},
		getSelectedBaseModelName: () => {
			// TODO update for selected > 3
			let name
			update(s => { name = s.baseModels[s.selectedBaseModel].metadata.name; return s })
			return name
		},
		getBaseModelNameFromIndex: (index) => {
			// TODO update for selected > 3
			let name
			update(s => { name = s.baseModels[index].metadata.name; return s})
			return name
		},
		getBaseModelIndexFromName: (name) => {
			// TODO update for selected > 3
			let _baseModels
			let index
			update(s => { _baseModels = s.baseModels; return s })
			switch (name) {
				case _baseModels[0].metadata.name: index = 0; break;
				case _baseModels[1].metadata.name: index = 1; break;
				case _baseModels[2].metadata.name: index = 2; break;
				case _baseModels[3].metadata.name: index = 3; break;
				default: index = -1; break;
			}
			if (index < 0) {
				// TODO: something
			}
			return index
		},
		// Get sculpt(ure) / model
		getSculpture: (index) => {
			let _sculpture
			update(s => {
				_sculpture = s.session.sculptures[index]
				return s
			})
			if (debug) console.log("[SessionStore] getSculpture(): index, sculpture", index, _sculpture)
			return _sculpture
		},
		getSculptById: (sculptId) => {
			let _sculptureIndex = methods.sculptureIndexFromId(sculptId)
			if (_sculptureIndex) {
				let _sculptIndex    = methods.sculptIndexFromId(sculptId)
				let _sculpt
				update(s => {
					_sculpt = s.session.sculptures[_sculptureIndex].sculpts[_sculptIndex]
					return s
				})
				if (debug) console.log("[SessionStore] getSculptById(): sculptId, sculpt", sculptId, _sculpt)
				return _sculpt
			} else return undefined
		},
		getModelBySculptId: (sculptId) => {
			let _sculptureIndex = methods.sculptureIndexFromId(sculptId)
			let _sculptIndex    = methods.sculptIndexFromId(sculptId)
			let _model
			if (_sculptureIndex != undefined) {
				update(s => {
					_model = s.session.sculptures[_sculptureIndex].sculpts[_sculptIndex].model
					return s
				})
				// if (debug) console.log("[SessionStore] getModelBySculptId(): _sculptureIndex, sculptId, model", _sculptureIndex, sculptId, _model)
				return _model
			} else {
				// if (debug) console.log("[SessionStore] getModelBySculptId(): _sculptureIndex, sculptId, model", _sculptureIndex, sculptId, _model)
				return undefined
			}
		},
		getLatestSculptId: () => {
			let _sculptureIndex, _sculpture, _sculptIndex, _sculpt
			update(s => {
				_sculptureIndex = s.selectedSculpture
				_sculpture      = s.session.sculptures[_sculptureIndex]
				if (_sculpture.sculpts.length > 0)
					_sculpt = _sculpture.sculpts[_sculpture.sculpts.length-1]
				else
					_sculpt = undefined
				return s
			})
			let latestSculpt = {
				sculptureIndex: _sculptureIndex,
				sculpture:      _sculpture,
				sculpt:         _sculpt
			}
			if (debug) console.log("[SessionStore] getLatestSculptId(): latestSculptId", latestSculpt)
			return _sculpt ? _sculpt.id : undefined
		},
		getSculptModelByBlockIndex: (blockIndex) => {
			let _sculptId
			update(s => { _sculptId = s.session.instrument.blocks[blockIndex].sculptId; return s })
			return methods.getModelBySculptId(_sculptId)
		},
		// Sculpting + Instrument update combined logic
		setupAndSetBlocks: async () => {
			let setupResult, sculptResult
			if (methods.sculptureSetupState()) {
				// setupResult  = await methods.sculptureSetup(methods.getSelectedBaseModelName())
				setupResult  = await methods.sculptureSetup(JSON.stringify(methods.getSelectedBaseModel()))
				sculptResult = await methods.sculptAndSetBlocks()
			} else {
				setupResult = undefined
				if (debug) console.log("[SessionStore] setupAndSetBlocks(): Can't do Setup! Add a Sculpture first.")
			}
			return setupResult
		},
		sculptureSetupState: () => {
			let setup_state
			update(s => {
				if (s.session.sculptures.length === 0)
					setup_state = false
				else if (s.session.sculptures[s.selectedSculpture].setup.baseModel.metadata.name === '')
					setup_state = true
				else setup_state = false
				return s
			})
			return setup_state
		},
		sculptAndSetBlocks: async () => {
			let res
			let setup_state
			update(s => {
				if (s.session.sculptures.length === 0)
					setup_state = false
				else if (s.session.sculptures[s.selectedSculpture].setup.baseModel.metadata.name != '')
					setup_state = true
				else setup_state = false
				return s
			})
			if (setup_state) {
				res = await methods.sculptureSculpt()
				methods.setInstrumentMode('play')
			} else {
				if (debug) console.log("[SessionStore] sculptAndSetBlocks(): Can't do Add Sculpt! Add and Setup a Sculpture first.")
			}
			return res
		},
		selectBlockAsBaseModel: (blockIndex) => {
			let blockSculptId, _selectedBaseModel
			update(s => {
				blockSculptId = s.session.instrument.blocks[blockIndex].sculptId
				s.selectedBaseModel = blockIndex + 4
				_selectedBaseModel = s.selectedBaseModel
				return s
			})
			let sculptModel = methods.getModelBySculptId(blockSculptId)
			for (var i = 0; i < instrument.blocks.length; i++)
				BelaRes.setModels(sculptModel)
			if (debug) console.log('[SessionStore] selectBlockAsBaseModel(): \nblockSculptId', blockSculptId, '\nsculptModel', sculptModel, '\nselectedBaseModel', _selectedBaseModel)
		},
		setBlockToSculptureBaseModel: (blockIndex, sculptureIndex) => {
			let _baseModel
			update(s => {
				_baseModel = s.session.sculptures[sculptureIndex].setup.baseModel
				return s
			})
			let baseModelIndex = methods.getBaseModelIndexFromName(_baseModel.metadata.name)
			if (debug) console.log("[SessionStore] setBlockToSculptureBaseModel(): blockIndex, sculptureIndex, baseModelIndex", blockIndex, sculptureIndex, baseModelIndex)
			methods.setBlockToBaseModel(blockIndex, baseModelIndex)
		},
		setBlocksToSculptureBaseModel: (sculptureIndex, blockIndexes=[0,1,2,3]) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.setBlockToSculptureBaseModel(blockIndexes[i], sculptureIndex)
		},
		// Instrument modes
		setInstrumentMode: (mode) => {
			switch (mode) {
				case 'play':    methods.setInstrumentPlayMode();    break;
				case 'compare': methods.setInstrumentCompareMode(); break;
				case 'preview': methods.setInstrumentPreviewMode(); break;
			}
			methods.saveInstrument()
		},
		setInstrumentPlayMode: () => {
			update(s => {
				s.session.instrument.mode.play    = true
				s.session.instrument.mode.compare = false
				s.session.instrument.mode.preview = false
				return s
			})
			methods.setUnlockedBlocksToLatestSculpt()
			methods.syncBlocksToBela()
		},
		setInstrumentCompareMode: () => {
			let _selectedSculpture
			update(s => {
				s.session.instrument.mode.play    = false
				s.session.instrument.mode.compare = true
				s.session.instrument.mode.preview = false
				_selectedSculpture = s.sessions.sculptures[s.sessions.sculptures.length-1]
				return s
			})
			// methods.compareSculptureWithBaseModel(_selectedSculpture, 0) // magic number
			// methods.matchingMode()
		},
		setInstrumentPreviewMode: () => {
			let _selectedBaseModel
			update(s => {
				s.session.instrument.mode.play    = false
				s.session.instrument.mode.compare = false
				s.session.instrument.mode.preview = true
				s.selectedBaseModel = 0
				_selectedBaseModel = s.selectedBaseModel
				return s
			})
			methods.setBlocksToBaseModelByIndex(_selectedBaseModel)
		},
		getInstrumentMode: () => {
			let _mode
			update(s => { 
				if (s.session.instrument.mode.play === true)
					_mode = 'play'
				else if (s.session.instrument.mode.compare === true)
					_mode = 'compare'
				else if (s.session.instrument.mode.preview === true)
					_mode = 'preview'
				else _mode = undefined
				return s
			})
			return _mode
		},
		// Set instrument block
		setBlock: (blockIndex, block) => {
			let _model = methods.getModelBySculptId(block.sculptId)
			update(s => {
				s.session.instrument.blocks[blockIndex] = block
				return s
			})
			// methods.saveInstrument()
			if (debug) console.log('[SessionStore] setBlock(): blockIndex, block, model:', blockIndex, block, _model)
			BelaRes.setPitch(blockIndex, block.pitch)
			if (_model) BelaRes.setModel(blockIndex, _model)
			// BelaFX.setFX(blockIndex, block.fx)
		},
		setBlockToSculptById: (blockIndex, sculptId) => {
			let _model = methods.getModelBySculptId(sculptId)
			let _block
			update(s => {
				s.session.instrument.blocks[blockIndex].sculptId = sculptId
				_block = s.session.instrument.blocks[blockIndex]
				return s
			})
			// methods.saveInstrument()
			if (debug) console.log('[SessionStore] setBlockToSculptById(): blockIndex, block, sculptId, model:', blockIndex, _block, sculptId, _model)
			if (_model) BelaRes.setModel(blockIndex, _model)
		},
		setBlockToPrevSculpt: (blockIndex) => {
			let currentSculptId, prevSculptId, prevSculptureLength
			update(s => {
				currentSculptId = s.session.instrument.blocks[blockIndex].sculptId
				return s
			})
			let currentSculptureIndex = methods.sculptureIndexFromId(currentSculptId)
			let currentSculptIndex = methods.sculptIndexFromId(currentSculptId)
			update(s => {
				if (currentSculptureIndex > 0)
					prevSculptureLength = s.session.sculptures[currentSculptureIndex-1].sculpts.length
				else prevSculptureLength = undefined
				return s
			})
			if (currentSculptIndex === 0) { // wrap to previous sculpture
				if (currentSculptureIndex === 0)// first sculpt of first sculpture
					prevSculptId = currentSculptId
				else if (currentSculptureIndex > 0)// last sculpt of previous sculpture
					prevSculptId = utils.prepZero((currentSculptureIndex-1).toString()) + utils.prepZero((prevSculptureLength-1).toString())
			} else if (currentSculptIndex > 0)
				prevSculptId = utils.prepZero((currentSculptureIndex).toString()) + utils.prepZero((currentSculptIndex-1).toString())
			if (debug) console.log('[SessionStore] setBlockToPrevSculpt(): blockIndex, currentSculptId, prevSculptId:', blockIndex, currentSculptId, prevSculptId)
			switch (methods.getInstrumentMode()) {
				case 'play':
					methods.setBlockToSculptById(blockIndex, prevSculptId)
					methods.saveInstrument()
					break
				case 'preview':
					methods.setBlockToSculptById(blockIndex, prevSculptId)
					methods.saveInstrument()
					let _model = methods.getModelBySculptId(prevSculptId)
					BelaRes.setModels(_model)
					break
				default: break
			}
		},
		setBlockToPrevSculpture: (blockIndex) => {
			let currentSculptId, prevSculptId, prevSculptureIndex, prevSculptIndex, prevSculptureLength
			update(s => {
				currentSculptId = s.session.instrument.blocks[blockIndex].sculptId
				return s
			})
			let currentSculptureIndex = methods.sculptureIndexFromId(currentSculptId)
			let currentSculptIndex    = methods.sculptIndexFromId(currentSculptId)
			update(s => {
				if (currentSculptureIndex > 0)
					prevSculptureLength = s.session.sculptures[currentSculptureIndex-1].sculpts.length
				else prevSculptureLength = undefined
				return s
			})
			if (currentSculptureIndex === 0)
				prevSculptId = currentSculptId
			else if (currentSculptureIndex > 0) {
				prevSculptureIndex = currentSculptureIndex - 1
				if (currentSculptIndex >= prevSculptureLength-1)
					prevSculptIndex = prevSculptureLength-1
				else
					prevSculptIndex = currentSculptIndex
				prevSculptId = utils.prepZero(prevSculptureIndex.toString()) + utils.prepZero(prevSculptIndex.toString())
			}
			if (debug) console.log('[SessionStore] setBlockToPrevSculpture(): blockIndex, currentSculptId, prevSculptId:', blockIndex, currentSculptId, prevSculptId)
			switch (methods.getInstrumentMode()) {
				case 'play':
					methods.setBlockToSculptById(blockIndex, prevSculptId)
					methods.saveInstrument()
					break
				case 'preview':
					methods.setBlockToSculptById(blockIndex, prevSculptId)
					methods.saveInstrument()
					let _model = methods.getModelBySculptId(prevSculptId)
					BelaRes.setModels(_model)
					break
				default: break
			}
		},
		setBlockToNextSculpt: (blockIndex) => {
			let currentSculptId, nextSculptId, currentSculptureLength, totalSculptures
			update(s => {
				totalSculptures = s.session.sculptures.length
				currentSculptId = s.session.instrument.blocks[blockIndex].sculptId
				return s
			})
			let currentSculptureIndex = methods.sculptureIndexFromId(currentSculptId)
			let currentSculptIndex = methods.sculptIndexFromId(currentSculptId)
			update(s => {
				currentSculptureLength = s.session.sculptures[currentSculptureIndex].sculpts.length
				return s
			})
			if (currentSculptIndex === currentSculptureLength-1) {// last sculpt
				if (currentSculptureIndex === totalSculptures-1) // last sculpture
					nextSculptId = currentSculptId 
				if (currentSculptureIndex < totalSculptures-1) // increment to first sculpt of next sculpture
					nextSculptId = utils.prepZero((currentSculptureIndex+1).toString()) + '00'
			} else if (currentSculptIndex < currentSculptureLength-1)
				nextSculptId = utils.prepZero((currentSculptureIndex).toString()) + utils.prepZero((currentSculptIndex+1).toString())
			if (debug) console.log('[SessionStore] setBlockToNextSculpt(): blockIndex, currentSculptId, nextSculptId:', blockIndex, currentSculptId, nextSculptId)
			switch (methods.getInstrumentMode()) {
				case 'play':
					methods.setBlockToSculptById(blockIndex, nextSculptId)
					methods.saveInstrument()
					break
				case 'preview':
					methods.setBlockToSculptById(blockIndex, nextSculptId)
					methods.saveInstrument()
					let _model = methods.getModelBySculptId(nextSculptId)
					BelaRes.setModels(_model)
					break
				default: break
			}
		},
		setBlockToNextSculpture: (blockIndex) => {
			let currentSculptId, nextSculptId, nextSculptureIndex, nextSculptIndex, nextSculptureLength, totalSculptures
			update(s => {
				totalSculptures = s.session.sculptures.length
				currentSculptId = s.session.instrument.blocks[blockIndex].sculptId
				return s
			})
			let currentSculptureIndex = methods.sculptureIndexFromId(currentSculptId)
			let currentSculptIndex = methods.sculptIndexFromId(currentSculptId)
			update(s => {
				if (currentSculptureIndex < totalSculptures-1)
					nextSculptureLength = s.session.sculptures[currentSculptureIndex+1].sculpts.length
				else nextSculptureLength = undefined
				return s
			})
			if (currentSculptureIndex === totalSculptures-1)
				nextSculptId = currentSculptId
			else if (currentSculptureIndex < totalSculptures-1) {
				nextSculptureIndex = currentSculptureIndex + 1
				if (currentSculptIndex > nextSculptureLength - 1) nextSculptIndex = nextSculptureLength - 1
				else nextSculptIndex = currentSculptIndex
				nextSculptId = utils.prepZero(nextSculptureIndex.toString()) + utils.prepZero(nextSculptIndex.toString())
			}
			if (debug) console.log('[SessionStore] setBlockToPrevSculpture(): blockIndex, currentSculptId, nextSculptId:', blockIndex, currentSculptId, nextSculptId)
			switch (methods.getInstrumentMode()) {
				case 'play':
					methods.setBlockToSculptById(blockIndex, nextSculptId)
					methods.saveInstrument()
					break
				case 'preview':
					methods.setBlockToSculptById(blockIndex, nextSculptId)
					methods.saveInstrument()
					let _model = methods.getModelBySculptId(nextSculptId)
					BelaRes.setModels(_model)
					break
				default: break
			}
		},
		// setBlocksToPrevSculpt: (blockIndexes=[0,1,2,3]) => {},
		// setBlocksToNextSculpt: (blockIndexes=[0,1,2,3]) => {},
		setBlockToPitch: (blockIndex, pitch) => {
			update(s => {
				s.session.instrument.blocks[blockIndex].pitch = pitch
				return s
			})
			if (debug) console.log('[SessionStore] setBlockToPitch(): blockIndex, pitch:', blockIndex, pitch)
			BelaRes.setPitch(blockIndex, pitch)
		},
		setBlockToFX: (blockIndex, fx) => {
			update(s => {
				s.session.instrument.blocks[blockIndex].fx = fx
				return s
			})
			if (debug) console.log('[SessionStore] setBlockToFX(): blockIndex, fx:', blockIndex, fx)
			// BelaFX.setFX(blockIndex, fx)
		},
		// Set instrument blocks
		setBlocks: (blocks, blockIndexes=[0,1,2,3]) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.setBlock(blockIndexes[i], blocks[i])
		},
		setBlocksToSculpts: (sculptIds, blockIndexes=[0,1,2,3]) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.setBlockToSculptById(blockIndexes[i], sculptIds[i])
		},
		setBlocksToPitches: (pitches, blockIndexes=[0,1,2,3]) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.setBlockToPitch(blockIndexes[i], pitches[i])
		},
		setBlocksToFX: (blockIndex, fx) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.setBlockToFx(blockIndexes[i], fx[i])
		},
		// Instrument blocks - special cases
		setBlockToLatestSculpt: (blockIndex) => {
			let latestSculptId = methods.getLatestSculptId()
			if (debug) console.log('[SessionStore] setBlockToLatestSculpt(): blockIndex, latestSculptId:', blockIndex, latestSculptId)
			methods.setBlockToSculptById(blockIndex, latestSculptId)
			return latestSculptId
		},
		setUnlockedBlocksToLatestSculpt: () => {
			let blockIndexes = []
			update(s => {
				for (var i = 0; i < s.session.instrument.blocks.length; i++)
					if (s.session.instrument.blocks[i].locked === false)
						blockIndexes.push(i)
				return s
			})
			if (debug) console.log('[SessionStore] setUnlockedBlocksToLatestSculpt(): blockIndexes:', blockIndexes)
			methods.setBlocksToLatestSculpt(blockIndexes)
		},
		setBlocksToLatestSculpt: (blockIndexes=[0,1,2,3]) => {
			let latestSculptId = methods.getLatestSculptId()
			if (debug) console.log('[SessionStore] setBlockToLatestSculpt(): blockIndexes, latestSculptId:', blockIndexes, latestSculptId)
			for (var i = 0; i < blockIndexes.length; i++)
				methods.setBlockToSculptById(blockIndexes[i], latestSculptId)
			return latestSculptId
		},
		setBlockToBaseModel: (blockIndex, baseModelIndex) => {
			let _baseModel
			update(s => { _baseModel = s.baseModels[baseModelIndex]; return s })
			if (_baseModel) BelaRes.setModel(blockIndex, _baseModel)
			return _baseModel
		},
		setBlocksToBaseModelByIndex: (baseModelIndex, blockIndexes=[0,1,2,3]) => {
			if (methods.sculptureSetupState()) {
				let _baseModel
				update(s => {
					s.selectedBaseModel = baseModelIndex
					_baseModel = s.baseModels[baseModelIndex];
					return s
				})
				if (_baseModel)
					for (var i = 0; i < blockIndexes.length; i++)
						BelaRes.setModel(blockIndexes[i], _baseModel)
				return _baseModel	
			} else console.log('[SessionStore] setBlocksToBaseModelByIndex() Choose New Sculpture first.')
		},
		setBlocksToBaseModelByName: (baseModelName, blockIndexes=[0,1,2,3]) => {
			let index = methods.getBaseModelIndexFromName(baseModelName)
			return methods.setBlocksToBaseModelByIndex(index, blockIndexes)
		},
		setBlocksToBaseModels: () => {
			let _baseModels
			update(s => { _baseModels = s.baseModels; return s })
			if (_baseModels) {
				for (var i = 0; i < _baseModels.length; i++) {
					BelaRes.setModel(i, _baseModels[i])
					// TODO: remove ref
					BelaRes.setPitch(i, "a3") // magic number
				}
			}
			if (debug) console.log('[SessionStore] setBlocksToBaseModels(): baseModels', _baseModels)
			return _baseModels
		},
		setBlocksToCompareLatestSculpture: () => {
			// TODO
			// Block 0: Base Model
			// Blocks 1-3: Last 3 sculpts
		},
		toggleBlockPitches: () => {
			let _block1Pitch
			let _blockPitches
			update(s => {
				_block1Pitch = s.session.instrument.blocks[0].pitch
				_blockPitches = [
					s.session.instrument.blocks[0].pitch,
					s.session.instrument.blocks[1].pitch,
					s.session.instrument.blocks[2].pitch,
					s.session.instrument.blocks[3].pitch
				]
				return s
			})
			switch(_blockPitches.toString()) {
				case 'c3,c3,c3,c3': methods.setBlocksToMonophic('a3'); break
				case 'a3,a3,a3,a3': methods.setBlocksToMonophic('g4'); break
				case 'g4,g4,g4,g4': methods.setBlocksToMonophic('d5'); break
				case 'd5,d5,d5,d5': methods.setBlocksToMultiphonic(); break
				case 'c3,a3,g4,d5': methods.setBlocksToMonophic('c3'); break
			}
			// if (_block1Pitch === 'a3') methods.setBlocksToMultiphonic()
			// else methods.setBlocksToMonophic('a3')
		},
		setBlocksToMonophic: (blockPitch) => {
			methods.setBlocksToPitches([blockPitch,blockPitch,blockPitch,blockPitch])
		},
		setBlocksToMultiphonic: () => {
			methods.setBlocksToPitches(['c3', 'a3', 'g4', 'd5'])
		},
		// Instrument block sculpt selection locking/unlocking
		toggleBlockLock: (blockIndex) => {
			let lockState
			update(s => { lockState = s.session.instrument.blocks[blockIndex].locked; return s })
			if (lockState === true)
				methods.unlockBlock(blockIndex)
			else
				methods.lockBlock(blockIndex)
			return lockState
		},
		toggleBlockLocks: (blockIndexes=[0,1,2,3]) => {
			let lockState
			update(s => { lockState = s.session.instrument.blocks[0].locked; return s })
			if (lockState === true) {
				for (var i = 0; i < blockIndexes.length; i++)
					methods.unlockBlock(blockIndexes[i])
			} else {
				for (var i = 0; i < blockIndexes.length; i++)
					methods.lockBlock(blockIndexes[i])
			}
		},
		lockBlock: (blockIndex) => {
			let _block
			update(s => {
				s.session.instrument.blocks[blockIndex].locked = true
				_block = s.session.instrument.blocks[blockIndex]
				return s
			})
			if (debug) console.log('[SessionStore] lockBlock(): blockIndex, block', blockIndex, _block)
			methods.saveInstrument()
		},
		lockBlocks: (blockIndexes=[0,1,2,3]) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.lockBlock(blockIndexes[i])
		},
		unlockBlock: (blockIndex) => {
			let _block
			update(s => {
				s.session.instrument.blocks[blockIndex].locked = false
				_block = s.session.instrument.blocks[blockIndex]
				return s
			})
			if (debug) console.log('[SessionStore] unlockBlock(): blockIndex, block', blockIndex, _block)
			methods.saveInstrument()
		},
		unlockBlocks: (blockIndexes=[0,1,2,3]) => {
			for (var i = 0; i < blockIndexes.length; i++)
				methods.unlockBlock(blockIndexes[i])
		},
		// Instrument FX
		// pitchUp:   () => {},
		// pitchDown: () => {},
		// incSculpt: () => {},
		// decSculpt: () => {},
		// incFx: (fx) => {},
		// decFx: (fx) => {},
		reset: () => { return set(state) }
	}

	return { subscribe, set, update, ...methods }

}

export const SessionStore = createSession()
