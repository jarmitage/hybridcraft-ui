import { writable } from "svelte/store"
import { BelaStore as Bela } from './belaStore.js'

const createBelaFX = () => {

/*
	// Preset manager

	let fxPreset = {
		gate: { threshold: 0.01 },
		eq: {},
		compression: {},
		gain: { in: 1.0, out: 5.0 }
	}

	let state = {
		inputs:         4,
		fxPresets:      [],
		selectedPreset: 0
	}

	// ...

	methods = {
		presetNew:     () => {},
		presetLoadAll: () => {},
		presetLoad:    () => {},
		presetSave:    () => {}
	}

*/

	let state = {
		gate: {
			threshold: { val: 0.01, min: 0, max: 1, step: 0.01 }
		},
		gain: {
			in:  { val: 1, min: 0, max: 2,  step: 0.2 },
			out: { val: 5, min: 0, max: 10, step: 0.2 }
		},
		eq: {},
		compression: {}
	}

	const { subscribe, set, update } = writable(state)

	const methods = {
		init: () => { /*  */ },
		setGate: (gate) => {
			let t = gate.threshold
			update(s => {
				t = methods.constrain(t, s.gate.threshold.min, s.gate.threshold.max)
				s.gate.threshold.val = t
				return s 
			})
			methods.updateFX('gate')
		},
		getGate: () => {
			let gate
			update(s => { gate = s.gate; return s })
			return gate
		},
		setEq: (eq) => { /*  */ },
		getEq: () => { /*  */ },
		setComp: (comp) => { /*  */ },
		getComp: () => { /*  */ },
		setGain: (gain) => {
			gain = {
				in:  methods.constrain(gain.in,  lim.gain.inMin,  lim.gain.inMax),
				out: methods.constrain(gain.out, lim.gain.outMin, lim.gain.outMax)
			}
			update(s => { s.gain = gain; return s })
			methods.updateFX('gain')
		},
		getGain: () => {
			let gain
			update(s => { gain = s.gain; return s })
			return gain
		},
		inc: (fx) => {
			switch (fx) {
				case 'gate':
					let t = methods.getGate().threshold
					t.val = methods.constrain(t.val+t.step, t.min, t.max)
					methods.setGate({threshold: t.val})
					break
				// case 'inGain':  break;
				// case 'outGain': break;
			}
			if (Bela.connected() && msg) Bela.sendControl(msg)
		},
		dec: (fx) => {

		},
		updateFX: (fx) =>{
			let msg = null
			switch (fx) {
				case 'gain': msg = { command: 'updateGain',  args: methods.getGain() }; break;
				case 'eq':   msg = { command: 'updateEQ',    args: methods.getEQ()   }; break;
				case 'comp': msg = { command: 'updateComp',  args: methods.getComp() }; break;
				case 'gate': msg = { command: 'updateGates', args: methods.getGate() }; break;
			}
			if (Bela.connected() && msg) Bela.sendControl(msg)
		},
		constrain: (input, min, max) => {
			return Math.min(Math.max(input, min), max)
		},
		reset: () => { return set(state) }
	}

	return { subscribe, set, update, ...methods }

}

export const BelaFXStore = createBelaFX()
