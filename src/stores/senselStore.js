import { writable } from 'svelte/store'
import { SessionStore as s } from '../stores/sessionStore.js'

const createSensel = () => {

	let state = {
		layout: {
			//  Row 1 ------------------------------------------------------------------
			0:  'b1PrevSculpt', 1: 'b1NextSculpture', 2: 'b2PrevSculpt', 3: 'b2NextSculpture', 
			4:  'b3PrevSculpt', 5: 'b3NextSculpture', 6: 'b4PrevSculpt', 7: 'b4NextSculpture',
			//  Row 2 ------------------------------------------------------------------
			8:  'b1PrevSculpture', 9: 'b1NextSculpt', 10: 'b2PrevSculpture', 11: 'b2NextSculpt', 
			12: 'b3PrevSculpture', 13: 'b3NextSculpt', 14: 'b4PrevSculpture', 15: 'b4NextSculpt',
			//  Row 3 ------------------------------------------------------------------
			16: 'togglePitches', 17: null, 18: 'pm1', 19: 'pm2', 20: 'pm3', 21: 'pm4', 22: null, 23: null,
			//  Row 4 ------------------------------------------------------------------
			24: 'addSculpture', 25: null, 26: 'bm1', 27: 'bm2', 28: 'bm3', 29: 'bm4', 30: 'setup', 31: 'addSculpt',
			// Lock
			32: 'lock1', 33: 'lock2', 34: 'lock3', 35: 'lock4'
		}
	}

	const { subscribe, set, update } = writable(state)

	const methods = {
		init: () => {},
		handle: async (args) => {
			switch (methods.getFn(args)) {
				// Sculpture
				case 'newSession':   await s.new();                break;
				case 'addSculpture': await s.sculptureAddTmp();    break;
				case 'setup':        await s.setupAndSetBlocks();  break;
				case 'addSculpt':    await s.sculptAndSetBlocks(); break;
				// Preset Models
				case 'pm1': s.setBlocksToBaseModelByIndex(0); break;
				case 'pm2': s.setBlocksToBaseModelByIndex(1); break;
				case 'pm3': s.setBlocksToBaseModelByIndex(2); break;
				case 'pm4': s.setBlocksToBaseModelByIndex(3); break;
				// Base Models
				case 'bm1': s.selectBlockAsBaseModel(0); break;
				case 'bm2': s.selectBlockAsBaseModel(1); break;
				case 'bm3': s.selectBlockAsBaseModel(2); break;
				case 'bm4': s.selectBlockAsBaseModel(3); break;
				// Blocks
				case 'b1PrevSculpt':    s.setBlockToPrevSculpt(0);    break;
				case 'b2PrevSculpt':    s.setBlockToPrevSculpt(1);    break;
				case 'b3PrevSculpt':    s.setBlockToPrevSculpt(2);    break;
				case 'b4PrevSculpt':    s.setBlockToPrevSculpt(3);    break;
				case 'b1NextSculpt':    s.setBlockToNextSculpt(0);    break;
				case 'b2NextSculpt':    s.setBlockToNextSculpt(1);    break;
				case 'b3NextSculpt':    s.setBlockToNextSculpt(2);    break;
				case 'b4NextSculpt':    s.setBlockToNextSculpt(3);    break;
				case 'b1PrevSculpture': s.setBlockToPrevSculpture(0); break;
				case 'b2PrevSculpture': s.setBlockToPrevSculpture(1); break;
				case 'b3PrevSculpture': s.setBlockToPrevSculpture(2); break;
				case 'b4PrevSculpture': s.setBlockToPrevSculpture(3); break;
				case 'b1NextSculpture': s.setBlockToNextSculpture(0); break;
				case 'b2NextSculpture': s.setBlockToNextSculpture(1); break;
				case 'b3NextSculpture': s.setBlockToNextSculpture(2); break;
				case 'b4NextSculpture': s.setBlockToNextSculpture(3); break;
				// // Blocks
				case 'lock1': s.toggleBlockLock(0); break;
				case 'lock2': s.toggleBlockLock(1); break;
				case 'lock3': s.toggleBlockLock(2); break;
				case 'lock4': s.toggleBlockLock(3); break;
				// case 'toggleLockAll': s.toggleBlockLocks(); break;
				// Block pitches
				case 'togglePitches': s.toggleBlockPitches(); break;
				// case 'selectAll':           Session.selectAllBlocks(); break;
				// case 'selectBlock1':        Session.selectBlock(0);    break;
				// case 'selectBlock2':        Session.selectBlock(1);    break;
				// case 'selectBlock3':        Session.selectBlock(2);    break;
				// case 'selectBlock4':        Session.selectBlock(3);    break;
				// case 'pitchUp':             Session.pitchUp();         break;
				// case 'pitchDown':           Session.pitchDown();       break;
				// // FX
				// case 'decGate':             Session.decFx('gate');     break;
				// case 'incGate':             Session.incFx('gate');     break;
				// case 'decInGain':           Session.decFx('inGain');   break;
				// case 'incInGain':           Session.incFx('inGain');   break;
				// case 'decOutGain':          Session.decFx('outGain');  break;
				// case 'incOutGain':          Session.incFx('outGain');  break;
			}	
		},
		getLayout: () => { let l; update(s => { l = s.layout; return s }); return l },
		getFn: (f) => {update(s => { f = s.layout[f-1]; return s }); return f },
		reset: () => { return set(state) }
	}

	return { subscribe, set, update, ...methods }

}

export const SenselStore = createSensel()
