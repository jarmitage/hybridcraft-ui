import { writable } from "svelte/store"

const createAPI = () => {

	let state = {
		defaultPort: 8888,
		port:        8890,
		address:     'http://localhost:',
		connected:   false,
		req:         null
	}

	const { subscribe, set, update } = writable(state)

	const methods = {
		init: async (port=null) => {
			if (port != null) update(s => { s.port = port; return s })
			return await methods.handshake()
		},
		handshake: async () => {
			let result = await methods.get('handshake')
			if (!result) {
				console.log('[APIStore] Could not connect to backend:', result)
			} else {
				console.log('[APIStore] Connected to backend.')
				update(s => { s.connected = true; return s })
			}
			return result
		},
		send: async ({ method, path, data }) => {
			let base
			update(s => { base = s.address+s.port; return s })

			const opts = { method, headers: {} }

			if (data) {
				opts.headers['Content-Type'] = 'application/json'
				opts.body = JSON.stringify(data)
			}

			// opts.headers['Access-Control-Allow-Origin'] = '*';
			
			return fetch(`${base}/${path}`, opts)
				.then(r => r.text())
				.then(json => {
					try { return JSON.parse(json) }
					catch (err) { return json }
				})
		},
		get: async (path) => {
			return methods.send({ method: 'GET', path });
		},
        put: async (path, data) => {
			return methods.send({ method: 'PUT', path, data });
		},
		post: async (path, data) => {
			return send({ method: 'POST', path, data });
		},
		del: async (path) => {
			return methods.send({ method: 'DELETE', path });
		},
		queryStr: (params) => {
			return Object.keys(params).map(key => key + '=' + params[key]).join('&')
		},
		req: async (req, args=null) => {
			let res
			if (!methods.reqInProgress(req)) {
				methods.reqStarted(req)
				if (args != null){
					res = await methods.get(req+'?'+methods.queryStr(args))
				}
				else res = await methods.get(req)
				methods.reqEnded()
			}
			return res
		},
		reqStarted: (req) => {
			update (s => { s.req = req; return s })
		},
		reqEnded: () => {
			update (s => { s.req = null; return s })
		},
		reqInProgress: (newReq) => {
			let req
			update (s => { req = s.req; return s })
			if (req != null) {
				console.log('[APIStore] Request already in progress:', req)
				console.log('[APIStore] Cannot make new request:', newReq)
			}
			return req
		},
		reset: () => { return set(state) }
	}

	return { subscribe, set, update, ...methods }

}

export const APIStore = createAPI()
