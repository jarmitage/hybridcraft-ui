
export function datetimeFolderFromPath(dir) {
	if (dir !== undefined) {
		let split = dir.split('/')
		let last = split[split.length-1]
		return last
	} else {
		return "[_index.js] datetimeFolderFromPath() Error! 'dir' string empty: " + dir
	}
}

// TODO: This format should be compatible with Python's Datetime
export function datetimeStr(){
	let d = new Date()
	let yr  = d.getFullYear()
	let mon = prepZero(d.getMonth() + 1) // 0 indexed
	let day = prepZero(d.getDate())
	let hr  = prepZero(d.getHours())
	let min = prepZero(d.getMinutes())
	let sec = prepZero(d.getSeconds())
	return yr+'-'+mon+'-'+day+'_'+hr+min+sec
}

export function prepZero(num) {
	if (num < 10) return '0' + num
	else return num
}

export function constrain (input, min, max) {
	return Math.min(Math.max(input, min), max)
}

export function datetimeStrToDate(str){
	if (str === '' || str === undefined) return ''
	return new Date(str.replace(/-/g,"/"))
}

export function isEmptyArray(arr) {
	return ( typeof arr != 'undefined' && arr instanceof Array )
}

export function isNonEmptyStr(str) {
	return ( typeof str === 'string' && str.length > 0 )
}

export function isEmptyStr(str) {
	return !isNonEmptyStr(str)
}

export function toCamelCase(str) {
    return str.substring(0, 1).toUpperCase() + str.substring(1)
}

export function baseModelIndexFromName(name) {
	let index
	switch(name) {
		case 'handdrum':       index = 0; break;
		case 'khol-5.m6':      index = 1; break;
		case 'metallic':       index = 2; break;
		case 'Mirdangam-4.m5': index = 3; break;
		default:               index = 0; break;
	}
	return index
}

export function serialize (obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}

